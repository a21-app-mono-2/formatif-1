package a21.climoilou.mono2.formatifs;
import a21.climoilou.mono2.formatifs.View;
import a21.climoilou.mono2.formatifs.Model;

public class Controller {

    public void agit() {
        int value = new Model().getValue();
        new View().affiche("la nouvelle valeur est " + value);
    }

    public static void main(String[] args) {
        new Controller().agit();
    }
}
